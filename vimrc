" Note: this vimrc file is optimized for a japanese keyboard on a debian system
" Use with caution 

" Basic settings ====================================================================={{{
" Stuffs... -------------------------------------------{{{
" All system-wide defaults are set in $VIMRUNTIME/debian.vim and sourced by
" the call to :runtime you can find below.  If you wish to change any of those

runtime! debian.vim

if has("syntax")
  syntax on
endif


" Have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Have Vim load indentation rules and plugins
" according to the detected filetype.
if has("autocmd")
  filetype plugin indent on
endif

set background=dark
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden		" Hide buffers when they are abandoned
set hlsearch
set showcmd
set number
set autoindent
fixdel "Not working, quickfix below
inoremap [3~ <esc>llxi

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif
" }}}

" Run pathogen add-on manager
execute pathogen#infect()

" Enable C++11 for syntastic
let g:syntastic_cpp_compiler_options=' -std=c++11'

" Highlight any lign that exceeds 80 characters
if &tw != 0
	highlight OverLength ctermbg=darkred ctermfg=white guibg=#592929
	match OverLength /\%>80v.\+/
endif

" Sets default indent to 4 spaces
set tabstop=4
set shiftwidth=4

" Auto fold on opening
set foldmethod=syntax
set foldlevel=1

set laststatus=2
set statusline=%f
set statusline=[%F] 
set statusline+=[%{strlen(&fenc)?&fenc:'none'},
set statusline+=%{&ff}]
set statusline+=%y
set statusline+=%h
set statusline+=%m
set statusline+=%r

"if count(g:pathogen_disabled, 'Fugitive') < 1
	set statusline+=%{fugitive#statusline()}
"endif

set statusline+=\ %=
set statusline+=Line:%l/%L[%3.p%%]
set statusline+=\ Col:%-2.2c
set statusline+=\ Buf:%n
set statusline+=%< 
set statusline+=\ [%-3.b][0x%-2.B]\ 

hi StatusLine term=reverse ctermfg=7 ctermbg=4 gui=bold,reverse
if version >= 700
	au InsertEnter * hi StatusLine term=reverse ctermfg=7 ctermbg=1 gui=undercurl	guisp=Magenta
	au InsertLeave * hi StatusLine term=reverse ctermbg=4 ctermfg=7	gui=bold,reverse
endif

" =====================================================}}}

" Personal info abbreviations ========================================================{{{
iabbrev ssig Sylvain "ginchan" Leclercq
iabbrev smail maisbiensurqueoui@gmail.com
iabbrev suri ginchansden.com
" ======================================================================}}}

" Key mapping ========================================================================{{{

" Basic mapping (noleader) +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++{{{

" Get those leaders going [;+]{{{
let mapleader = ";"
noremap <leader>; ;
let maplocalleader = "+"
" }}}

" Get the .vimrc where I can see it <l>v[es]{{{
nnoremap <leader>ve :vsplit $MYVIMRC<cr>
nnoremap <leader>vs :source $MYVIMRC<cr>
" }}}

" Go back to normal mode easily (jk), <esc> does a quicksave {{{
inoremap jk <esc>l
vnoremap jk <esc>
inoremap <esc> <esc>:w<cr>i
nnoremap <esc> :w<cr>
" }}}

" Get rid of those annoying finger dragging arrows  {{{
noremap <up> <nop>
noremap <down> <nop>
noremap <right> <nop>
noremap <left> <nop>
" }}}

" Arrow like keyboard config [jkil] (cuz im no savage) {{{
noremap j h
noremap i k
noremap k j
noremap J \|
noremap L $
" }}}

" Group edition commands [sh] {{{
nnoremap S A
nnoremap s a
nnoremap A I
nnoremap a i
nnoremap h s
nnoremap H S
" }}}

" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++}}}

" Edition mapping <l>e +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++{{{

" Maps the hyphen (-) to moving a line down one line {{{
" And underscore to do the reverse operation (DNU on first line, it would only
" delete the line)
nnoremap \ ddp
nnoremap _ ddkP
" }}}

" Join and break lines <l>e[Jj] {{{
nnoremap <leader>j i<cr><esc>kg_
nnoremap <leader>J J
" }}}

" Add empty lines <l>[oO] {{{
nnoremap <leader>o mzo<esc>`z
nnoremap <leader>O mzO<esc>`z
"}}}

" Put a word between meaningfull symbols <l>e["<'] {{{
nnoremap <leader>e" viW<esc>a"<esc>hbi"<esc>lel
nnoremap <leader>e' viW<esc>a'<esc>hbi'<esc>lel
nnoremap <leader>e< viW<esc>a><esc>hbi<<esc>lel
vnoremap <leader>e" <esc>`<i"<esc>`>a"<esc>
vnoremap <leader>' <esc>`<i'<esc>`>a'<esc>
" }}}

" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++}}}

" Window management <l>w +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++{{{

" Open/close windows <l>wo[hv) <l>q {{{
nnoremap <leader>woh :split<cr>:e 
nnoremap <leader>wov :vsplit<cr>:e 
nnoremap <leader>q :q<cr>
" }}}

"Move around <l>w[wjkli] {{{
nnoremap <leader>ww <c-w><c-w>
nnoremap <leader>wk <c-w>j
nnoremap <leader>wj <c-w>h
nnoremap <leader>wl <c-w>l
nnoremap <leader>wi <c-w>k
" }}}

"Manipulate windows <l>w[+-] {{{
nnoremap <leader>w+ <c-w>+
nnoremap <leader>w- <c-w>-
" }}}

" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++}}}

" Buffer management <l>b +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++{{{

" Basic ops <l>b[ebld] {{{
nnoremap <leader>b :e
nnoremap <leader>be :e 
nnoremap <leader>bl :ls<cr>
nnoremap <leader>bd :bn<cr>:bd #<cr>
" }}}

" Movement <l>b[bnN] {{{
nnoremap <leader>bb :b#<cr>
nnoremap <leader>bn :bn<cr>
nnoremap <leader>bN :bp<cr>
" }}}

" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++}}}

" Search engine <l>g | <nol> +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++{{{

" Basic operations [/?], <l>nh {{{
nnoremap / /\v
nnoremap ? ?\v
nnoremap <leader>mh :nohl<cr>
inoremap <leader>mh <esc>:nohl<cr>i
" }}}

" Folder wide search on cword <l>[gGnN] {{{
nnoremap <leader>g :set operatorfunc=<SID>GrepOperator<cr>g@
vnoremap <leader>g :<c-u>call <SID>GrepOperator(visualmode())<cr>

function! s:GrepOperator(type)
	let saved_unnamed_register = @@
	if a:type ==# 'v'
		execute "normal! `<v`>y"
	elseif a:type ==# "char"
		execute "normal! `[v`]y"
	else
		return
	endif

	silent execute "grep! -R " . shellescape(@@) . " ."
	copen
	let @@ = saved_unnamed_register
endfunction

nnoremap <leader>n :cnext<cr>
nnoremap <leader>N :cp<cr>
" }}}

" Easy access to substr <l>s {{{
nnoremap <leader>s :s/\v
" }}}

" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++}}}

" Folding management f[aAcCoOrRmMjJiIkKlL] +++++++++++++++++++++++++++++++++++++++++++++++{{{
nnoremap fa za
nnoremap fA zA
nnoremap fc zc
nnoremap fC zC
nnoremap fo zo
nnoremap fO zO
nnoremap fr zr
nnoremap fR zR
nnoremap fm zm
nnoremap fM zM
nnoremap fj zc
nnoremap fi zk
nnoremap fl zo
nnoremap fk zj
" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++}}}

" =====================================================================================}}}

" Filetype specific commands ========================================================={{{

" Something specific for html folding hehe {{{
augroup dpsg_filetype_html
	autocmd!
	autocmd FileType html nnoremap <buffer> <localleader>f Vatzf
	autocmd FileType html setlocal foldmethod=manual
	autocmd FileType html,erb inoremap <buffer> </ </<c-x><c-o>
augroup END

" }}}

" Vimscript file settings {{{

augroup dpsg_filetype_vim 
	autocmd!
	" leader + f will add a folding level to a bloc of text, starting on the
	"last comment line and ending before the next empty line
	autocmd FileType vim nnoremap <buffer> <leader>f ?"<cr>A {{{<esc>/^$<cr>A" }}}<esc>:nohl<cr>za"
	"Workaround to the filetype issue (sourcing the file resets foldmethod and
	"foldlevel)
	autocmd FileType vim nnoremap <buffer> <leader>@ :set foldmethod=marker<cr>:set foldlevel=0<cr>:nohl<cr>
	autocmd FileType vim setlocal foldmethod=marker
	autocmd FileType vim iabbrev <buffer> == ==#
	autocmd FileType vim setlocal foldlevel=0
	autocmd FileType vim nnoremap <buffer> <leader>c :call ToggleComment('"')<cr>
augroup END
" }}}

" =====================================================================================}}}
